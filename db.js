const sqlite3 = require('sqlite3');
const db = new sqlite3.Database('urls.db');

db.run(`
    CREATE TABLE IF NOT EXISTS urls (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        originalUrl TEXT NOT NULL
    )
`);

module.exports = db;