const express = require('express');
const router = express.Router();
require('dotenv').config();
const db = require('./db');
const crypto = require('crypto-js');
const fs = require('fs');
const path = require('path');

/**
 * @swagger
 * tags:
 *   name: URL Shortener
 *   description: API for URL shortening
 */

/**
 * @swagger
 * /shorten:
 *   post:
 *     summary: Shorten a URL
 *     tags: [URL Shortener]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               originalUrl:
 *                 type: string
 *     responses:
 *       200:
 *         description: Shortened URL created successfully
 */
router.post('/shorten', (req, res) => {
    const { originalUrl } = req.body;
    db.run('INSERT INTO urls (originalUrl) VALUES (?)', [originalUrl], 
        function (err) {
            if (err) {
                return res.status(500).json({ error: 'Internal Server Error - INSERT' });
            }
    
            const shortId = this.lastID; 
            const shortUrl = generateShortUrl(shortId.toString());
    
            res.json({ 
                message: 'Shortened URL created successfully',
                shortUrl: `${req.protocol}://${req.get('host')}/r/${shortUrl}`
            });
    });
});

/**
 * @swagger
 * /delete:
 *   delete:
 *     summary: Delete a short URL
 *     tags: [URL Shortener]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               shortUrl:
 *                 type: string
 *     responses:
 *       200:
 *         description: Short URL deleted successfully
 *       400:
 *         description: Invalid Short URL format
 *       404:
 *         description: Short URL not found
 */
 router.delete('/delete', (req, res) => {
    const { shortUrl } = req.body;
    const shortUrlRegex = /^https?:\/\/[a-zA-Z0-9:.-]+\/r\/[a-zA-Z0-9_-]+$/;
    if (!shortUrlRegex.test(shortUrl)) {
        return res.status(400).json({ error: 'Invalid short URL format' });
    }

    const encryptedId = shortUrl.split('/r/')[1];
    const id = decrypt(encryptedId, crypto.enc.Utf8.parse(process.env.CRYPTO_KEY));
  
    db.get('SELECT * FROM urls WHERE id = ?', [id], (queryErr, row) => {
        if (queryErr || !row) {
            return res.status(404).json({ error: 'Short URL not found' });
        }
  
        db.run('DELETE FROM urls WHERE id = ?', [id], (deleteErr) => {
            if (deleteErr) {
                return res.status(500).json({ error: 'Internal Server Error - DELETE' });
            }
    
                res.json({ message: 'Short URL deleted successfully' });
            });
    });
});

/**
 * @swagger
 * /r/{shortUrl}:
 *   get:
 *     summary: Redirect to the original URL
 *     tags: [URL Shortener]
 *     parameters:
 *       - in: path
 *         name: shortUrl
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       302:
 *         description: Redirect to the original URL
 *       404:
 *         description: Short URL not found
 */
 router.get('/r/:shortUrl', (req, res) => {
    const { shortUrl } = req.params;
    const id = decrypt(shortUrl, crypto.enc.Utf8.parse(process.env.CRYPTO_KEY));
    
    db.get('SELECT originalUrl FROM urls WHERE id = ?', [id], (queryErr, row) => {
        if (queryErr || !row) {
            return res.status(404).json({ error: 'Short URL not found' });
        }
  
        res.redirect(row.originalUrl);
    });
});

/**
 * @swagger
 * /logs:
 *   get:
 *     summary: Streams the application logs file
 *     tags: [URL Shortener]
 *     responses:
 *       200:
 *         description: Application log file
 */
 router.get('/logs', (req, res) => {
    const logFileName = 'app.log';
  
    fs.access(logFileName, fs.constants.F_OK, (err) => {
        if (err) {
            return res.status(500).json({ error: 'Log file not available' });
        }
    
        const logFilePath = path.join(__dirname, logFileName);
    
        res.setHeader('Content-Type', 'text/plain');
        res.setHeader('Content-Disposition', `attachment; filename=${logFileName}`);
    
        const fileStream = fs.createReadStream(logFilePath);
        fileStream.pipe(res);
    });
});

function generateShortUrl(shortId) {
    return encrypt(shortId, crypto.enc.Utf8.parse(process.env.CRYPTO_KEY));
}

function encrypt(data, key) {
    return base64ToUrlSafe(Buffer.from(
        crypto.TripleDES.encrypt(data, key, { mode: crypto.mode.ECB, padding: crypto.pad.Pkcs7 }).toString())
        .toString('base64'));
}

function decrypt(encryptedData, key) {
    const decodeB64 = Buffer.from(urlSafeToBase64(encryptedData), 'base64').toString('utf8');
    return crypto.TripleDES.decrypt(decodeB64, key, { mode: crypto.mode.ECB, padding: crypto.pad.Pkcs7 })
        .toString(crypto.enc.Utf8);
}

function base64ToUrlSafe(base64) {
    return base64.replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
}
  
function urlSafeToBase64(urlSafeBase64) {
    urlSafeBase64 = urlSafeBase64.replace(/-/g, '+').replace(/_/g, '/');
    while (urlSafeBase64.length % 4) {
        urlSafeBase64 += '=';
    }
    return urlSafeBase64;
}

module.exports = router;