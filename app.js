const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT;

const db = require('./db');
const { specs, swaggerUi } = require('./swagger');

const morgan = require('morgan');
var fs = require('fs');
var path = require('path');
var appLogStream = fs.createWriteStream(path.join(__dirname, 'app.log'), { flags: 'a' });

app.use((req, res, next) => {
    const oldWrite = res.write;
    const oldEnd = res.end;
  
    const chunks = [];
  
    res.write = (...restArgs) => {
        chunks.push(Buffer.from(restArgs[0]));
        oldWrite.apply(res, restArgs);
    };
  
    res.end = (...restArgs) => {
        if (restArgs[0]) {
            chunks.push(Buffer.from(restArgs[0]));
        }
    
        const body = Buffer.concat(chunks).toString('utf8');
        req.logResponseBody = body;
    
        oldEnd.apply(res, restArgs);
    };
  
    next();
});

morgan.token('body', (req, res) => JSON.stringify(req.body));
morgan.token('response', (req, res) => req.logResponseBody);
app.use(morgan(
    ':remote-addr [:date[clf]] ":method :url HTTP/:http-version" :status | Request: :body | Response: :res[content-length] :response - :response-time ms', 
    { 
        stream: appLogStream,
        skip: (req, res) => res.statusCode < 400
    }
));
app.use(morgan(
    ':remote-addr [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] - :response-time ms', 
    { 
        stream: appLogStream,
        skip: (req, res) => res.statusCode >= 400
    }
));

app.use(express.json());

const routes = require('./routes');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
app.use(routes);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});